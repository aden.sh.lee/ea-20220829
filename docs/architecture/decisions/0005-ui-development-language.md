# 5. UI Development Language

Date: 2021-04-02

## Status

Proposed

## Context

- SPA 개발에 ES6, Typescript 2가지 옵션이 있음
- ES6 장점
    1. 별다른 교육이 필요없음.
    2. Vue2에는 ES6를 공식 지원함
- ES6 단점
    1. compile시 type checking이 없어, 런타임시 에러 발생할 수 있음.
    2. Vue3에서는 Typescript가 공식 채택되어 기술부채가 됨
- Typescript 장점
    1. React, Vue3 에서 공식적으로 채택
    2. compile시 type checking으로 run time error 를 줄일 수 있음
- Typescript 단점
    1. 문법을 익혀야 한다.
- Vue2에 Typescript을 적용한 사례가 많이 있어, 적응에 필요한 것 빼고는 문제되지 않음
- Typescript에 능숙한 홍길동책임이 있음

## Decision

Typescript로 개발한다.

## Consequences

Vue2 + Typescript 표준 코드 구조를 잡고 개발을 시작한다.
