# 13. version-control-system

Date: 2023-04-10

## Status

Accepted

## Context

TTA 아카데미 모델링 수업.

## Decision

버전관리 시스템을 gitlab으로 사용.

## Consequences

gitlab 계정을 만들고 필요한 파일을 등록.
