# 15. Use vscode server

Date: 2023-04-10

## Status

Accepted

Supersedes [14. dev-environment](0014-dev-environment.md)

## Context

무료버전 사용기간 만료.

## Decision

사내 vs server 설치 사용.

## Consequences

사내 전파.
